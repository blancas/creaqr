/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jiripinkas.sitemonitoring.controller;

import cz.jiripinkas.sitemonitoring.entity.CheckQR; 
import java.util.Map; 
import java.util.StringTokenizer;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Luis Blancas
 */
@ManagedBean
@ViewScoped 
public class QRController {
    private static final long serialVersionUID = 1L;
	private CheckQR check = new CheckQR();

    public CheckQR getCheck() 
    {
        this.check = new CheckQR();
        String dato="";
        FacesContext facesContext = FacesContext. getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        Map params=null;
        params = externalContext.getRequestParameterMap();
        try
        {
            dato= (String) params.get("dato");
            System.out.println("dato:"+dato);
            String decripter = EncryptUtil.decryptDES(dato);
            System.out.println("decripter  :"+decripter);
            StringTokenizer st= new StringTokenizer(decripter,"|");
            System.out.println("dato.count: :"+st.countTokens());
            int cont=st.countTokens();
            if(cont ==3)
            {
                System.out.println("dato creado >>>");
                this.check.setNumero(st.nextToken());
                this.check.setMonto(st.nextToken());
                this.check.setConcepto(st.nextToken());
                System.out.println("dato creado >>>" +  this.check.toString());
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return this.check;
    }

    public void setCheck(CheckQR check) {
        this.check = check;
    }
}
