/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jiripinkas.sitemonitoring.controller;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext; 
@ManagedBean
@ViewScoped 
public class RecepcionMB 
{
    private String url;

    public String getUrl() 
    {
        String dato="";
        FacesContext facesContext = FacesContext. getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        Map params=null;
        params = externalContext.getRequestParameterMap();
        try
        {
            dato= (String) params.get("dato");
            
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return "http://34.74.249.135:8080/qr.xhtml?dato="+dato;
    }
    public void regresa()
        {
            try 
            { 
                FacesContext context = FacesContext.getCurrentInstance();
                context.getExternalContext().redirect("index.xhtml?faces-redirect=true");
            } catch (IOException ex) {
                Logger.getLogger(CheckController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    public void setUrl(String url) {
        this.url = url;
    }
    
}
