package cz.jiripinkas.sitemonitoring.controller;
import java.io.Serializable;
import javax.faces.bean.ManagedBean; 
import javax.faces.bean.ViewScoped; 
import lombok.Getter;
import lombok.Setter;
import cz.jiripinkas.sitemonitoring.entity.CheckQR; 
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
@Getter
@Setter
@ManagedBean
@ViewScoped
public class CheckController implements Serializable 
{
	private static final long serialVersionUID = 1L;
	private CheckQR check = new CheckQR();
	public void clear()
        {
		check = new CheckQR();
	}
        public void crear(CheckQR check)
        {
            System.out.println("Entro ");
            try 
            {
                String dato =  check.getNumero()+"|"+
                        check.getMonto() +"|"+
                        check.getConcepto();
                System.out.println("dato creado "+dato);
                 System.out.println("check creado "+check.toString());
                 System.out.println("encripter dato creado "+EncryptUtil.encryptDES(dato));
                FacesContext context = FacesContext.getCurrentInstance();
                context.getExternalContext().redirect("recepcion.xhtml?faces-redirect=true&dato="+EncryptUtil.encryptDES(dato));
            } catch (IOException ex) {
                Logger.getLogger(CheckController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

}
