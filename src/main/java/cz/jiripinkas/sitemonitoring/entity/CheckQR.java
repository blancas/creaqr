package cz.jiripinkas.sitemonitoring.entity;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class CheckQR{

	private String numero;
        private String monto;
        private String concepto;
        private String nombreBene;
        private String CuentaBene;
        private String bancoBene;

    @Override
    public String toString() {
        return "CheckQR{" + "numero=" + numero + ", monto=" + monto + ", concepto=" + concepto + ", nombreBene=" + nombreBene + ", CuentaBene=" + CuentaBene + ", bancoBene=" + bancoBene + '}';
    }
        

}
